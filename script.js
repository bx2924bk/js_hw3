function avg(inputArray) {
    if (inputArray.length === 0){
        return 0;
    }
    let result = 0;
    for (i = 0; i < inputArray.length; i++){
        result = result + inputArray[i];
    }
    return result / inputArray.length;
}

let a = [1, 25 ,12, 8, 96, 34, 15];
console.log(avg(a));
